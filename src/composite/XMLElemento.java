/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;


/**
 *
* @author luisburgos, elsy
 */
public class XMLElemento extends XMLComponente{

    public XMLElemento() {}
    
    public XMLElemento(String nombreTag) {
        super(nombreTag);
    }

    @Override
    public void imprimirComponente() {        
        System.out.println("NombreTag : " + getNombreTag()
                + " | Valor: " +  (getValor() == null ? "Sin valor" : getValor()));
        imprimirAtributos();
    }

    @Override
    public XMLComponente buscarComponentePorNombreTag(String nombreTag) { 
        //System.out.println("actual: " + getNombreTag() + ",  buscando: " + nombreTag);
        if(getNombreTag().equalsIgnoreCase(nombreTag)){
            return this;
        }else{
            return null;    
        }        
    }
    
}
