/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
* @author luisburgos, elsy
 */
public abstract class XMLComponente {

    protected String nombreTag;
    protected String valor;
    protected XMLComponente contenedor;
    protected HashMap<String, String> atributos = new HashMap();
    
    protected int currIndexChild = 0;

    public XMLComponente() {
        atributos = new HashMap();
    }

    public XMLComponente(String nombreTag) {
        nombreTag = nombreTag;
        atributos = new HashMap();
    }

    public void setNombreTag(String nombreTag) {
        this.nombreTag = nombreTag;
    }

    public String getNombreTag() {
        return nombreTag;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
    
    public XMLComponente getContenedor(){
        return contenedor;
    }
    
    public void setContenedor(XMLComponente contenedor){
        this.contenedor = contenedor;
    }

    public void setAtributos(HashMap<String, String> atributos) {
        this.atributos = atributos;
    }

    public HashMap<String, String> getAtributos() {
        return atributos;
    }

    public boolean hasAtributos() {
        //if(atributos != null){
        return !atributos.isEmpty();
        //}
        //return false;
    }

    public Iterator<String> obtenerInteradorDeAtributos() {
        return getAtributos().keySet().iterator();
    }
    
    public void agregarAtributo(String attr, String valor) {
        atributos.put(attr, valor);
    }
    
    public void imprimirAtributos() {
        if (hasAtributos()) {
            System.out.println("    Atributos : ");
            for (Iterator iterator = obtenerInteradorDeAtributos(); iterator.hasNext();) {
                String llave = (String) iterator.next();
                System.out.println("        Atributo: " + llave + ", Valor: " + atributos.get(llave));
            }
        }
    }

    public void agregarComponente(XMLComponente componente) {
        throw new UnsupportedOperationException();
    }

    public Iterator<XMLComponente> obtenerIteradorComponentes() {
        throw new UnsupportedOperationException();
    }

    public void imprimirComponente() {
        throw new UnsupportedOperationException();
    }

    public XMLComponente buscarComponentePorNombreTag(String nombreTag) {
        throw new UnsupportedOperationException();
    }    
    
    public void resetChildCount(){
    	currIndexChild = 0;
    }
    
    public XMLComponente obtenerSiguienteGemelo(){
        XMLComponente contenedor = this.getContenedor();
    	if (contenedor != null){
	    	XMLComponente itNodo = null;
	    	Iterator<XMLComponente> ij = contenedor.obtenerIteradorComponentes(); 
	    	int counter = 0;
	    	while (ij.hasNext()){
	   /*     	if (this.getNombreTag() == "alumno"){
	        		System.out.println(currIndexChild + ", cnt: " + counter);
	        	}*/
	    		itNodo = ij.next();
	    		if (counter > currIndexChild){
	    			if (this.getNombreTag() == itNodo.getNombreTag()){
	    				currIndexChild = counter;
	    				return itNodo;
	    			}
	    		}
	    		counter++;
	        }
    	}
    	return null;
    }

}
