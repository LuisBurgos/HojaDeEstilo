/*  
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
* @author luisburgos, elsy
 */
public class XMLCompuesto extends XMLComponente {

    private ArrayList<XMLComponente> xmlComponents;
    
    public XMLCompuesto() {
        super();
        xmlComponents = new ArrayList<XMLComponente>();
    }
    
    public XMLCompuesto(String nombreTag) {
        super(nombreTag);
        xmlComponents = new ArrayList<XMLComponente>();
    }

    @Override
    public void agregarComponente(XMLComponente componente) {
        componente.setContenedor(this);
        this.xmlComponents.add(componente);
    }

    @Override
    public void imprimirComponente() {
        System.out.println("NombreTag : " + getNombreTag()
                + " | Valor: " +  (getValor() == null ? "Sin valor" : getValor()));
        imprimirAtributos();
        xmlComponents.stream().forEach((xmlComponent) -> {
            xmlComponent.imprimirComponente();
        });
    }

    @Override
    public XMLComponente buscarComponentePorNombreTag(String nombreTag) {
        XMLComponente componenteEncontrado = null;   
        boolean encontrado = false;        
        //System.out.println("actual: " + getNombreTag() + ",  buscando: " + nombreTag);
        if (getNombreTag().equals(nombreTag)) {
            componenteEncontrado = this;            
        } else {
            for (Iterator<XMLComponente> iterator = obtenerIteradorComponentes(); iterator.hasNext();) {               
                XMLComponente xmlComponent = iterator.next();
                if(xmlComponent.buscarComponentePorNombreTag(nombreTag) != null && !encontrado){
                    componenteEncontrado = xmlComponent;                    
                    //break;
                    encontrado = true;
                }
            }         
            
            if(!encontrado){
                componenteEncontrado = this;
            }
        }
        return componenteEncontrado;                   
    }
    
    @Override
    public Iterator<XMLComponente> obtenerIteradorComponentes() {
        return xmlComponents.iterator();
    }
    
}
