/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author luisburgos
 */
public class XMLTestDrive {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        XMLComponente listado = new XMLCompuesto();
        listado.setNombreTag("listado");
        listado.setValor("Listado de Notas");        
        //listado.setAtributos(null);
        
        XMLComponente asignatura = new XMLElemento();
        asignatura.setNombreTag("asignatura");
        asignatura.setValor(null);
        HashMap<String, String> atributosAsignatura = new HashMap();
        atributosAsignatura.put("nombre", "Estructuras de Datos II");
        atributosAsignatura.put("curso", "04-05");
        atributosAsignatura.put("plan", "1996");
        asignatura.setAtributos(atributosAsignatura);
        
        
        XMLComponente fecha = new XMLElemento();
        fecha.setNombreTag("fecha");
        fecha.setValor("02/02/05");
        HashMap<String, String> atributosFecha = new HashMap();
        atributosFecha.put("formato", "dd/mm/aa");
        fecha.setAtributos(atributosFecha);
        
        XMLComponente notas = new XMLCompuesto();
        notas.setNombreTag("notas");
        notas.setValor(null);
        //notas.setAtributos(null);
        
            XMLComponente alumno;

            alumno = new XMLElemento();
            alumno.setNombreTag("alumno");
            alumno.setValor(null);
            HashMap<String, String> atributosAlumno;

                atributosAlumno = new HashMap();
                atributosAlumno.put("nombre", "Jose Gonzalez Perez");
                atributosAlumno.put("dni", "20094785");
                atributosAlumno.put("nota", "8.5");
                alumno.setAtributos(atributosAlumno);
                notas.agregarComponente(alumno);

                atributosAlumno = new HashMap();
                atributosAlumno.put("nombre", "Juan Garcia Perez");
                atributosAlumno.put("dni", "70898989");
                atributosAlumno.put("nota", "7.8");
                alumno.setAtributos(atributosAlumno);
                notas.agregarComponente(alumno);

                atributosAlumno = new HashMap();
                atributosAlumno.put("nombre", "Antonio Blanco Moreno");
                atributosAlumno.put("dni", "89859999");
                atributosAlumno.put("nota", "5.0");
                alumno.setAtributos(atributosAlumno);
                notas.agregarComponente(alumno);
        
        listado.agregarComponente(asignatura);
        listado.agregarComponente(fecha);
        listado.agregarComponente(notas);
        
        //listado.imprimirComponente();       
        listado.buscarComponentePorNombreTag("notas").imprimirComponente();
        
        
    }
    
}
