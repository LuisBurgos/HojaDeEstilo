/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
* @author luisburgos, elsy
 */
public class Nodo {
        
    private String nombreTag;
    private String valor;
    private HashMap<String, String> atributos = new HashMap();
    private List<Nodo> hijos  = new ArrayList<Nodo>();
    private Nodo padre;
    
    public Nodo(){}
    private int currIndexChild = 0;
    
    
    public Nodo(Nodo nodo){ 
        nombreTag = nodo.getNombreTag();        
    }
    
    public Nodo(String contenido){
        this.setNombreTag(contenido);
    }    
    
    public void agregarNodo(Nodo nodo){
        nodo.padre = this;
        hijos.add(nodo);
    }
    
     public void setHijos(List<Nodo> hijos) {
        if(hijos != null){
            for(Nodo hijo : hijos) {
               hijo.padre = this;
            }
        }

        this.hijos = hijos;
    }
     
    public void setAtributos(HashMap atributos){
        this.atributos = atributos;
    }
    
    public HashMap<String, String> getAtributos(){
        return this.atributos;
    }
    
    public void imprimirContenido(){
        if(hijos != null){
            hijos.stream().forEach((hijo) -> {
                
                System.out.println(hijo.nombreTag.toString() + hijo.atributos.toString() 
                        + " / " + hijo.padre.getNombreTag() + hijo.getValor());
                hijo.imprimirContenido();
            });
        }
    }
            
    public void setNombreTag(String contenido){
        this.nombreTag = contenido;
    }
    
    public String getNombreTag(){
        return this.nombreTag;
    }
    
    public Nodo getPadre(){
        return this.padre;
    }
    
    public void setPadre(Nodo padre){
        this.padre = padre;
    }
    
    public List<Nodo> getHijos(){
        return this.hijos;
    }
    
    public int getNumeroDeHijos() {
        return getHijos().size();
    }

    public boolean hasHijos() {
        return (getNumeroDeHijos() > 0);
    }
    
    public String getValor(){
        return valor;
    }

    public void setValor(String contenido) {
        this.valor = contenido;
    }
    
    public Nodo getChildNode(String tagName){
    	Nodo nodo = null;
    	boolean found = false;
    	//System.out.println("nodeName" + getNombreTag() + ",  tagname:" + tagName);
    	if (this.hasHijos()){
        	//System.out.println("tagname again:" + tagName);
    		for (Iterator<Nodo> ij = this.getHijos().iterator(); ij.hasNext() && !found;) {
                 Nodo o2 = ij.next();
                 if (o2.getNombreTag().equals(tagName)){
                	 nodo = o2;
                	 found = true;
                 }
             }
    		if (!found){
    			nodo = this;
    		}
    	}else{
    		nodo = this;
    	}
    	return nodo;
    }
    
    public void resetChildCount(){
    	currIndexChild = 0;
    }
    
    public Nodo getNextTwin(){
    	Nodo padre = this.getPadre();
    	if (padre != null){
	    	Nodo itNodo = null;
	    	Iterator<Nodo> ij = padre.getHijos().iterator(); 
	    	int counter = 0;
	    	while (ij.hasNext()){
	   /*     	if (this.getNombreTag() == "alumno"){
	        		System.out.println(currIndexChild + ", cnt: " + counter);
	        	}*/
	    		itNodo = ij.next();
	    		if (counter > currIndexChild){
	    			if (this.getNombreTag() == itNodo.getNombreTag()){
	    				currIndexChild = counter;
	    				return itNodo;
	    			}
	    		}
	    		counter++;
	        }
    	}
    	return null;
    }
}
