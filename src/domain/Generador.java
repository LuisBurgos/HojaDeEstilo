/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import composite.XMLComponente;

import java.io.File;

/**
 *
* @author luisburgos, elsy
 */
public class Generador {
    
    private static Generador instanciaGenerador;
    
    private Generador (){ }
    
    public static Generador getInstance(){
        if (instanciaGenerador == null) {
            instanciaGenerador = new Generador();
        }
        return instanciaGenerador;
    }
    /*
    public void generaSalidaHTML(Arbol listadoArbol, File file){                
        System.out.println("Comenzando...");
        Parser parser = new Parser();
        parser.parsearHojaEstilos(listadoArbol);
        //raiz.imprimirContenido();
        //System.out.println(file.toString());
    }*/
    
    public void generarHTML(XMLComponente listado, String file){                
        System.out.println("Comenzando...");
        Parser parser = new Parser(file);
        parser.parsearHojaEstilos(listado);
        //raiz.imprimirContenido();
        //System.out.println(file.toString());
    }      
}
