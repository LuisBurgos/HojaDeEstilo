/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import composite.XMLComponente;
import java.util.Iterator;

/**
 *
* @author luisburgos, elsy
 */
public class Arbol {

    private Nodo raiz;
    private XMLComponente xmlRaiz;

    public Arbol() {
        super();
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }
    
    public void setRaiz(Nodo raiz, XMLComponente xmlRaiz){
        this.raiz = raiz;
        this.xmlRaiz = xmlRaiz;
    }

    public Nodo obtenerNodoRaiz() {
        return this.raiz;
    }

    public XMLComponente obtenerXMLRaiz(){
        return this.obtenerXMLRaiz();
    }
    
    public void imprimirArbol() {

        System.out.println(raiz.getNombreTag() + " : " + raiz.getValor());
        for (Iterator<Nodo> it = obtenerNodoRaiz().getHijos().iterator(); it.hasNext();) {
            Nodo o = it.next();
            System.out.println(o.getNombreTag() + " : " + o.getValor());

            if (o.getHijos() != null) {
                for (Iterator<Nodo> ij = o.getHijos().iterator(); ij.hasNext();) {
                    Nodo o2 = ij.next();
                    System.out.println("    " + o2.getNombreTag());
                    //System.out.println("padre" + o2.getPadre().getNombreTag());
                    
                    if (o2.getAtributos() != null) {
                        for (Iterator im = o2.getAtributos().keySet().iterator(); im.hasNext();) {
                            String key = (String) im.next();
                            System.out.println("        " + key + ":" + o2.getAtributos().get(key));
                        }
                    }
                }
            }

            if (o.getAtributos() != null) {
                for (Iterator ij = o.getAtributos().keySet().iterator(); ij.hasNext();) {
                    String key = (String) ij.next();
                    System.out.println("    " + key + ":" + o.getAtributos().get(key));
                }
            }

        }

    }
}
