package domain;

import java.util.HashMap;
import java.util.Scanner;

import composite.XMLComponente;
import composite.XMLCompuesto;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DocumentReader;

/**
 *
* @author luisburgos, elsy
 */
public class Parser {

    private String contenidoDelHTML;
    private DocumentReader documentReader;
    //private static final String NOMBRE_ARCHIVO = "hojaestilo.pl";
    private Scanner scanner;

    private final String FILE_PATH = "informe.html";
    private PrintWriter bw;

    public Parser(String nombre) {
        documentReader = new DocumentReader();
        contenidoDelHTML = documentReader.obtenerContenidoPorNombreDeArchivo(nombre);
        scanner = new Scanner(contenidoDelHTML);
    }

    private String reemplazarVariables(String originalContent, Nodo currentNode) {
        String newContent = originalContent;
        boolean more = true;
        int startKeyCommand = 0;
        int endKeyCommand = 0;
        startKeyCommand = newContent.indexOf("@");
        while (startKeyCommand > 0) {
            endKeyCommand = newContent.indexOf("@", startKeyCommand + 1);
            //System.out.println(startKeyCommand + " - " + endKeyCommand);
            String cmd = newContent.substring(startKeyCommand + 1, endKeyCommand);

            //System.out.println("cmd: " + cmd + ":" + currNode.getAtributos().get(cmd));
            if (cmd.equals("")) {
                //System.out.println("...-");
                newContent = newContent.replace("@@", currentNode.getValor());
            } else {
                newContent = newContent.replace("@" + cmd + "@", currentNode.getAtributos().get(cmd.toLowerCase()));
            }
            //System.out.println("cmd: " + cmd + ":" + currNode.getAtributos().get(cmd));
            startKeyCommand = newContent.indexOf("@", endKeyCommand + 1);
        }
        return newContent;
    }

    private void buscarElemento(String contenido, Nodo currentNode) {
        int startKeyCommand = contenido.indexOf("{");
        if (startKeyCommand < 0) {
            if (contenido.contains("@")) {
                currentNode.resetChildCount();
                Nodo twinNode = currentNode;
                do {
                    String toInyect = reemplazarVariables(contenido, twinNode);
                    inyectarHTML(toInyect);
                    System.out.println(toInyect);
                    //System.out.println(twinNode.getNombreTag());
                    //System.out.println(twinNode.getAtributos().toString());
                    twinNode = currentNode.getNextTwin();
                } while (twinNode != null);

            } else {
                inyectarHTML(contenido);
                System.out.println(contenido);
            }
            return;
        }
        buscarElemento(contenido.substring(0, startKeyCommand), currentNode);///
        int startContent = startKeyCommand + contenido.substring(startKeyCommand).indexOf("<");
        String cmdOpen = contenido.substring(startKeyCommand + 1, startContent);
        String childNodeName = cmdOpen.toLowerCase().trim();
        String cmdClose = "}" + cmdOpen.trim();
        cmdOpen = "{" + cmdOpen;
        int startCommand = contenido.indexOf(cmdOpen);
        int endCommand = contenido.indexOf(cmdClose);
        //System.out.println(contenido.substring(startCommand,startCommand+cmdOpen.length()));
        buscarElemento(contenido.substring(startCommand + cmdClose.length(), endCommand), currentNode.getChildNode(childNodeName));///
        //System.out.println(contenido.substring(endCommand,endCommand+cmdClose.length()));
        buscarElemento(contenido.substring(endCommand + cmdClose.length()), currentNode);///
    }

    private String reemplazarVariables(String originalContent, XMLComponente currentComponente) {
        String newContent = originalContent;
        boolean more = true;
        int startKeyCommand = 0;
        int endKeyCommand = 0;
        startKeyCommand = newContent.indexOf("@");
        while (startKeyCommand > 0) {
            endKeyCommand = newContent.indexOf("@", startKeyCommand + 1);
            //System.out.println(startKeyCommand + " - " + endKeyCommand);
            String cmd = newContent.substring(startKeyCommand + 1, endKeyCommand);

            //System.out.println("cmd: " + cmd + ":" + currentComponente.getAtributos().get(cmd.toLowerCase()));
            if (cmd.equals("")) {
                //System.out.println("...-");
                newContent = newContent.replace("@@", currentComponente.getValor());
            } else {
                //if(!currentComponente.hasAtributos()){ System.out.println("No atributos");}
                newContent = newContent.replace("@" + cmd + "@", currentComponente.getAtributos().get(cmd.toLowerCase()));
            }
            //System.out.println("cmd: " + cmd + ":" + currentComponente.getAtributos().get(cmd));
            startKeyCommand = newContent.indexOf("@", endKeyCommand + 1);
        }
        return newContent;
    }

    private void buscarElemento(String contenido, XMLComponente currentComponente) {
        int startKeyCommand = contenido.indexOf("{");
        if (startKeyCommand < 0) {
            if(currentComponente instanceof XMLCompuesto){
                currentComponente = (XMLCompuesto)currentComponente;
            }
            if (contenido.contains("@")) {
                currentComponente.resetChildCount();
                XMLComponente componenteGemelo = currentComponente;
                //System.out.println(currentComponente.getNombreTag());
                do {
                    String toInyect = reemplazarVariables(contenido, componenteGemelo);
                    inyectarHTML(toInyect);
                    //System.out.println(toInyect);
                    //System.out.println(twinNode.getNombreTag());
                    //System.out.println(twinNode.getAtributos().toString());
                    componenteGemelo = currentComponente.obtenerSiguienteGemelo();
                } while (componenteGemelo != null);

            } else {
                inyectarHTML(contenido);
                //System.out.println(contenido);
            }
            return;
        }
        buscarElemento(contenido.substring(0, startKeyCommand), currentComponente);///
        int startContent = startKeyCommand + contenido.substring(startKeyCommand).indexOf("<");
        String cmdOpen = contenido.substring(startKeyCommand + 1, startContent);
        String childNodeName = cmdOpen.toLowerCase().trim();
        String cmdClose = "}" + cmdOpen.trim();
        cmdOpen = "{" + cmdOpen;
        int startCommand = contenido.indexOf(cmdOpen);
        int endCommand = contenido.indexOf(cmdClose);
        //System.out.println(contenido.substring(startCommand,startCommand+cmdOpen.length()));        
        if(currentComponente.buscarComponentePorNombreTag(childNodeName) != null){
            buscarElemento(contenido.substring(startCommand + cmdClose.length(), endCommand), currentComponente.buscarComponentePorNombreTag(childNodeName));
        }///
        //System.out.println(contenido.substring(endCommand,endCommand+cmdClose.length()));
        buscarElemento(contenido.substring(endCommand + cmdClose.length()), currentComponente);///
    }

    public void parsearHojaEstilos(Arbol listadoArbol) {
        //System.out.println(parser.contenidoDelHTML);
        buscarElemento(contenidoDelHTML, listadoArbol.obtenerNodoRaiz());
        //parser.tokenizar();
    }

    public void parsearHojaEstilos(XMLComponente listado) {
        //System.out.println(parser.contenidoDelHTML);
        buscarElemento(contenidoDelHTML, listado);
        //parser.tokenizar();
    }

    private void inyectarHTML(String html) {

        try {
            bw = new PrintWriter(new FileWriter(FILE_PATH, true));
            bw.println(html);
            bw.close();
        } catch (Exception ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
