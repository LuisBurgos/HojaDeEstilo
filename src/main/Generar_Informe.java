/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import utils.DocumentReader;
import adapter.DocumentAdapter;
import adapter.XMLAdapter;
import composite.XMLComponente;
import domain.Generador;
import utils.Validaciones;

/**
 *
* @author luisburgos, elsy
 */
public class Generar_Informe {
    public static void main(String[] args) {      
        Validaciones validaciones = new Validaciones(args);
        if(validaciones.isValid()){
            
            DocumentReader dr = new DocumentReader();
            DocumentAdapter documentAdapter = new XMLAdapter();            
            
            String xml = "";
            String estilos = "";
            
            for(String string : args){
                if(string.contains(".xml")){
                    xml = string;
                }else if(string.contains(".pl")){
                    estilos = string;
                }else{
                    //Do nothing
                }
            }
            
            documentAdapter.generarArbol(dr.obtenerArchivo(xml)); 
            XMLComponente listado = documentAdapter.obtenerXML();
            
            Generador generador = Generador.getInstance();
            generador.generarHTML(listado, estilos);
        }
    }
}
