package exceptions;

/**
 *
* @author luisburgos, elsy
 */
public class MissingParametersException extends Exception{
    
    public MissingParametersException(){
        super();
    }
    
    public MissingParametersException(String message) { 
        super("Numero de parametros incorrectos"); 
    }
    
    public MissingParametersException(String message, Throwable cause) { 
        super(message, cause); 
    }
    
    public MissingParametersException(Throwable cause) { 
        super(cause); 
    }
}
