/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import java.io.FileNotFoundException;

/**
 *
* @author luisburgos, elsy
 */
public class FileNotInCurrentDirectoryException extends Exception{
    
    public FileNotInCurrentDirectoryException(){
        super();
    }
    
    public FileNotInCurrentDirectoryException(String message) { 
        super("Archivo no encontrado en el directorio actual"); 
    }
    
    public FileNotInCurrentDirectoryException(String message, Throwable cause) { 
        super(message, cause); 
    }
    
    public FileNotInCurrentDirectoryException(Throwable cause) { 
        super(cause); 
    }
    
}
