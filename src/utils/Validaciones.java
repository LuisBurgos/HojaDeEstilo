/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import exceptions.FileNotInCurrentDirectoryException;
import exceptions.MissingParametersException;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisburgos
 */
public class Validaciones {
       
    private  final int NUM_PARAMETROS = 2;
    private  String[] directorio;    
    private boolean statusProcess;    
    
    public Validaciones(String[] args){
       directorio = args;
    }
        
    public boolean isValid(){      
        try {
            validarNumeroDeParametros();
            validarExistenciaEnDirectorio();            
            statusProcess= true;            
        } catch (MissingParametersException | FileNotInCurrentDirectoryException ex) {
            statusProcess=false;
            Logger.getLogger(Validaciones.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return statusProcess;
    }    
    
    private void validarNumeroDeParametros() throws MissingParametersException{
        if(directorio.length != NUM_PARAMETROS){            
            throw new MissingParametersException();
        }        
    }
    
    private void validarExistenciaEnDirectorio() throws FileNotInCurrentDirectoryException {                        
        //String currentDirectory = System.getProperty("user.dir") /*+ "/src/assets/"*/;
        String currentDirectory = "./";
        File file;
        for(String parametro : directorio){
            file = new File(currentDirectory + parametro);
            
            if(!file.exists()){
                throw new FileNotInCurrentDirectoryException();
             
            }
        }                
    }
    
    
    
    
}
