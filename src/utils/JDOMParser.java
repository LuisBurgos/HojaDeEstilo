/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import domain.Arbol;
import domain.Nodo;
import composite.XMLComponente;
import composite.XMLCompuesto;
import composite.XMLElemento;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;


/**
 *
 * @author luisburgos, elsy
 */
public class JDOMParser {

    private Arbol arbol;
    private XMLComponente arbolDatos;
    private File xmlFile;
    
    public JDOMParser() {
        
    }
    
    public void setFile(File file){
        this.xmlFile = file;
    }
    
    public void cargarArbol(){
        try {
            SAXBuilder builder = new SAXBuilder();
            Document document = (Document) builder.build(xmlFile);
            cargarJDOM(document);    
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void cargarJDOM(Document document) {
        arbol = new Arbol();
        arbolDatos = new XMLCompuesto(); ///XMLOComponente
        Element rootTree = document.getRootElement();
        Nodo rootNode = new Nodo();
        rootNode.setNombreTag(rootTree.getName());
        rootNode.setValor(rootTree.getTextTrim());
        cargarNodo(rootTree, rootNode);
        cargarNodoXML(rootTree, rootNode, arbolDatos);
        arbol.setRaiz(rootNode);
        arbolDatos.setNombreTag(rootTree.getName());  ///XMLOComponente
        arbolDatos.setValor(rootTree.getTextTrim());  ///XMLOComponente
    }
    
    private void cargarNodo(Element arbol, Nodo nodo){
    	  List list = null;
    	  list = arbol.getChildren();
          for (Iterator it = list.iterator(); it.hasNext();) {
              Element currNode = (Element) it.next();
              Nodo nodoHijo = new Nodo();
              nodoHijo.setNombreTag(currNode.getName());
              nodoHijo.setValor(currNode.getTextTrim());
              HashMap attributosHijo = new HashMap();
              if (!currNode.getAttributes().isEmpty()) {
            	  List att = currNode.getAttributes();
                  for (Iterator is = att.iterator(); is.hasNext();) {
                      Attribute o = (Attribute) is.next();
                      attributosHijo.put(o.getName(), o.getValue());
                  }
                  nodoHijo.setAtributos(attributosHijo);
              }
              if (!currNode.getChildren().isEmpty()) {
            	  cargarNodo(currNode,nodoHijo);
              }            
              nodo.agregarNodo(nodoHijo);
         }
    }
    
    private void cargarNodoXML(Element arbol, Nodo nodo, XMLComponente componente){
    	  List list = null;
    	  list = arbol.getChildren();
          for (Iterator it = list.iterator(); it.hasNext();) {
              Element currNode = (Element) it.next();
              Nodo nodoHijo = new Nodo();
              
              XMLComponente xmlHijo; ///XMLOComponente
              if(!currNode.getChildren().isEmpty()){ 
                  xmlHijo = new XMLCompuesto(); 
              }else{
                  xmlHijo = new XMLElemento();
              }                    
              
              nodoHijo.setNombreTag(currNode.getName());
              nodoHijo.setValor(currNode.getTextTrim());
              xmlHijo.setNombreTag(currNode.getName());///XMLOComponente
              xmlHijo.setValor(currNode.getTextTrim());///XMLOComponente
              
              HashMap attributosHijo = new HashMap();
              if (!currNode.getAttributes().isEmpty()) {
            	  List att = currNode.getAttributes();
                  for (Iterator is = att.iterator(); is.hasNext();) {
                      Attribute o = (Attribute) is.next();
                      attributosHijo.put(o.getName(), o.getValue());
                  }
                  nodoHijo.setAtributos(attributosHijo);
                  xmlHijo.setAtributos(attributosHijo);///XMLOComponente
              }
              if (!currNode.getChildren().isEmpty()) {
            	  cargarNodoXML(currNode,nodoHijo, xmlHijo);
              }            
              nodo.agregarNodo(nodoHijo);
              componente.agregarComponente(xmlHijo);///XMLOComponente
         }
    }
    
    public Arbol getArbol(){
        return this.arbol;
    }
   
    public XMLComponente obtenerRaizArbolDatos(){
        return this.arbolDatos;
    }
    
  }