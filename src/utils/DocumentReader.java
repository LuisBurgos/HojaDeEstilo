/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisburgos, elsy
 */


public class DocumentReader {
    
    
    //private static final String FILE_PATH="src/assets/";
    
    public File obtenerArchivo(String nombre){
        return new File( /*FILE_PATH + */nombre);        
    }
    
    public String obtenerContenidoPorNombreDeArchivo(String nombre){
        
        String contenidoHojaDeEstilo = "";
        String lineaDelArchivo;
        FileReader file = null;

        try {
            file = new FileReader(/*FILE_PATH + */nombre);
            BufferedReader buffer = new BufferedReader(file);
            
            while((lineaDelArchivo = buffer.readLine()) != null) {
                contenidoHojaDeEstilo = contenidoHojaDeEstilo + lineaDelArchivo ;
            }
            
            buffer.close();
        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DocumentReader.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(DocumentReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return contenidoHojaDeEstilo;
    }
    
}
