/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import domain.Arbol;
import composite.XMLComponente;
import java.io.File;
import utils.JDOMParser;

/**
 *
* @author luisburgos, elsy
 */
public class XMLAdapter implements DocumentAdapter{
    
    JDOMParser jdomParser;
    
    public XMLAdapter(){
    }
    
    @Override
    public void generarArbol(File file) {
        jdomParser = new JDOMParser();
        jdomParser.setFile(file);        
        jdomParser.cargarArbol();        
    }

    @Override
    public Arbol obteberArbol() {
        return jdomParser.getArbol();
    }
    
    @Override
    public XMLComponente obtenerXML(){
        return jdomParser.obtenerRaizArbolDatos();
    }
    
}
