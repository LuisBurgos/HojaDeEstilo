/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

import domain.Arbol;
import composite.XMLComponente;
import java.io.File;

/**
 *
* @author luisburgos, elsy
 */
public interface DocumentAdapter {    
    public void generarArbol(File file);    
    public Arbol obteberArbol();
    public XMLComponente obtenerXML();
}
